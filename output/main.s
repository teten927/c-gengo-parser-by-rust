		.text
		.align 2
main:
		addi $sp $sp -20
		sw $ra 0($sp)
		li $s0 2
		li $s1 4
		li $s2 30
		li $s3 3
		add $t1 $s0 $s1
		mul $t1 $t1 $s1
		li $t4 2
		mul $t1 $t1 $t4
		div $t4 $s2 $s1
		add $t4 $s0 $t4
		li $t3 1
		div $t4 $t3 $t4
		add $t1 $t1 $t4
		move $s4 $t1
		li $s5 3
		li $s6 20
		li $t1 6
		addi $t1 $s6 6
		mul $t1 $s3 $t1
		mul $t1 $s1 $t1
		mul $t1 $s0 $t1
		mul $t1 $s2 $t1
		li $t4 136
		addi $t1 $t1 -136
		move $s7 $t1
		bge $s0 $s1 label1
		add $t1 $s0 $s1
		move $s2 $t1
		li $t1 34
		bge $s2 $t1 label2
		li $t1 2
		addi $t1 $s4 2
		move $s0 $t1
		j label3
label2:
		li $s1 4
label3:
		j label4
label1:
		move $s2 $s0
label4:
		bge $s0 $s1 label5
		add $t1 $s0 $s1
		move $s0 $t1
label5:
		sw $s5 4($sp)
		li $s5 0
loop_label6:
		li $t1 32
		bge $s5 $t1 label6
		add $t1 $s2 $s5
		move $s2 $t1
		li $t1 1
		addi $t1 $s5 1
		move $s5 $t1
		sw $s6 8($sp)
		li $s6 3
loop_label7:
		li $t1 20
		bge $s6 $t1 label7
		li $s2 1
loop_label8:
		li $t1 2
		bge $s2 $t1 label8
		li $t1 2
		addi $t1 $s4 2
		move $s4 $t1
		li $t1 1
		addi $t1 $s2 1
		move $s2 $t1
		j loop_label8
label8:
		li $t1 2
		addi $t1 $s6 2
		move $s6 $t1
		j loop_label7
label7:
		j loop_label6
label6:
loop_label9:
		li $t1 32
		bge $s2 $t1 label9
		li $t1 3
		addi $t1 $s2 3
		move $s2 $t1
		j loop_label9
label9:
		sw $s3 12($sp)
		li $s3 0
loop_label10:
		li $t1 32
		bge $s3 $t1 label10
		add $t1 $s2 $s5
		move $s2 $t1
		li $s5 0
		li $t1 1
		addi $t1 $s3 1
		move $s3 $t1
		j loop_label10
label10:
		li $s5 0
loop_label11:
		li $t1 32
		bge $s5 $t1 label11
		li $t1 3
		addi $t1 $s2 3
		move $s2 $t1
		li $t1 1
		addi $t1 $s5 1
		move $s5 $t1
		j loop_label11
label11:
		mul $t1 $s7 $s5
		sw $s1 16($sp)
		lw $s1 4($sp)
		mul $t1 $t1 $s1
		move $v1 $t1
		j main_end
main_end:
		lw $ra 0($sp)
		addi $sp $sp 20
		j $ra

		.data
		.align 2
