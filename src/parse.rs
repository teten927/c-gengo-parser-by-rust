use lex::Token;
use error_handler::ErrorInfo;

impl Token {
    fn return_val(&self) -> (&str, Option<String>) {
        match self {
            Token::Variable(x)      => ("VARIABLE", Some(x.to_string())),
            Token::Figure(x)        => ("FIGURE", Some(x.to_string())),
            Token::Arithmetic(x)
             if &*x == "+" || 
                &*x == "-"          => ("ADDORSUB", Some(x.to_string())),
            Token::Arithmetic(x)
             if &*x == "*" || 
                &*x == "/" ||
                &*x == "%"          => ("MULTIORDIV", Some(x.to_string())),
            Token::Comparison(x)    => ("COMPARISON", Some(x.to_string())),
            Token::Type(x)          => ("TYPE", Some(x.to_string())),
            Token::VOID             => ("VOID", None),
            Token::WHILE            => ("WHILE", None),
            Token::FOR              => ("FOR", None),
            Token::IF               => ("IF", None),
            Token::ELSE             => ("ELSE", None),
            Token::EQ               => ("EQ", None),
            Token::L_P              => ("L_P", None),
            Token::R_P              => ("R_P", None),
            Token::L_CB             => ("L_CB", None),
            Token::R_CB             => ("R_CB", None),
            Token::L_SB             => ("L_SB", None),
            Token::R_SB             => ("R_SB", None),
            Token::COMMA            => ("COMMA", None),
            Token::SEMICOL          => ("SEMICOL", None),
            Token::RETURN           => ("RETURN", None),
            _                       => ("", None),
        }
    }
}

#[derive (Debug, Clone)]
pub enum Node {
    Number(i32, ErrorInfo),
    Variable(String, ErrorInfo),
    FuncName(String, ErrorInfo),
    Type(String, ErrorInfo),
    Branch {
        left: Option<Box<Node>>,
        right: Option<Box<Node>>,
        branch: (String, ErrorInfo)
    },
    StatusNode(ErrorInfo),
    None
}

impl Node {
    fn fetch_error_info(&self) -> ErrorInfo {
        match self {
            Node::Number(_, error_info) => (*error_info).clone(),
            Node::Variable(_, error_info) => (*error_info).clone(),
            Node::FuncName(String, error_info) => (*error_info).clone(),
            Node::Type(String, error_info) => (*error_info).clone(),
            Node::Branch { branch: _branch, ..} => { _branch.1.clone() },
            Node::StatusNode(error_info) => (*error_info).clone(),
            Node::None => ErrorInfo::new(0,0)
        }
    }
}

#[derive (Debug, Clone)]
pub struct ParseNode {
    node: Node,
    status: String,
    lock: bool
}

impl ParseNode {
    fn new(token_info: (Token, ErrorInfo)) -> ParseNode {
        match token_info.0.return_val() {
            (x @ "VARIABLE", Some(value)) => {
                ParseNode {
                    node: Node::Variable(value, token_info.1),
                    status: x.to_string(),
                    lock: false
                }
            },
            (x @ "FIGURE", Some(value)) => {
                ParseNode {
                    node: Node::Number(value.parse().unwrap(), token_info.1),
                    status: x.to_string(),
                    lock: false
                }
            },
            (x @ "TYPE", Some(value)) => {
                ParseNode {
                    node: Node::Type(value.parse().unwrap(), token_info.1),
                    status: x.to_string(),
                    lock: false
                }
            },
            (x @ _, Some(value)) => {
                ParseNode {
                    node: Node::Branch {
                        left: None,
                        right: None,
                        branch: (value, token_info.1)
                    },
                    status: x.to_string(),
                    lock: false
                }
            },
            (x @ _, None) => {
                ParseNode {
                    node: Node::StatusNode(token_info.1),
                    status: x.to_string(),
                    lock: false
                }
            }
        }
    }
}

enum NodeOrIndex {
    node(Node),
    index(usize),
    None
}

#[derive (Debug)]
pub struct ParseNodes {
    nodes: Vec<ParseNode>,
    begin: usize,
    end: usize,
    tmp_assign_nodes: Vec<ParseNode>
}

impl Iterator for ParseNodes {
    type Item = ParseNode;
    fn next(&mut self) -> Option<ParseNode> {
        if self.end < self.nodes.len() {
            let node = self.nodes[self.end].clone();
            self.end += 1;
            Some(node)
        } else {
            self.end += 1;
            None
        }
    }
}

impl ParseNodes {
    fn new() -> ParseNodes {
        ParseNodes {
            nodes: vec![],
            begin: 0,
            end: 0,
            tmp_assign_nodes: vec![]
        }
    }
    fn array_push(&mut self, node: ParseNode) {
        self.nodes.push(node);
    }
    fn init(&mut self) {
        self.begin = 0;
        self.end = 0;
    }
    fn back(&mut self) {
        self.end = self.begin;
    }
    fn increment(&mut self) {
        self.begin += 1;
    }
    fn fetch_parse_node_by_index(&mut self, index: usize) -> ParseNode {
        self.nodes[self.begin + index].clone()
    }
    fn set_parase_node_by_index(&mut self, index: usize, node: ParseNode) {
        self.nodes[self.begin + index] = node;
    }
    fn insert_parse_node_by_index(&mut self, index: usize, node: ParseNode) {
        self.nodes.insert(self.begin + index, node);
    }
    fn is_locked_node_by_index(&mut self, index: usize) -> bool {
        self.nodes[self.begin + index].lock
    }
    fn set_lock_node_by_index(&mut self, index: usize) {
        self.nodes[self.begin + index].lock = true;
    }
    fn is_match(&mut self, comp:Vec<&str>) -> bool {
        self.back();
        let mut iter = comp.iter();
        while let Some(x) = self.next() {
            match iter.next() {
                Some(y) if y.to_string() == x.status => {},
                None => {return true;},
                _ => {return false;}
            }
        }
        if let None = iter.next() {return true}
        else {return false}
    }
    fn make_node(&mut self, left_node_enum: NodeOrIndex, right_node_enum: NodeOrIndex, branch: &str, status: &str) 
        ->  ParseNode {
        let mut left_node: Option<Box<Node>>;
        let mut right_node: Option<Box<Node>>;
        let mut error1: Option<ErrorInfo> = None;
        let mut error2: Option<ErrorInfo> = None;
        match left_node_enum {
            NodeOrIndex::node(x) => {
                left_node = Some(Box::new(x.clone()));
                error1 = Some(x.fetch_error_info());
            },
            NodeOrIndex::index(x) => {
                let tmp_node = self.fetch_parse_node_by_index(x).node;
                left_node = Some(Box::new(tmp_node.clone()));
                error1 = Some(tmp_node.fetch_error_info());
            },
            NodeOrIndex::None => {
                left_node = None;
            }
        }
        match right_node_enum {
            NodeOrIndex::node(x) => {
                right_node = Some(Box::new(x.clone()));
                error2 = Some(x.fetch_error_info());
            },
            NodeOrIndex::index(x) => {
                let tmp_node = self.fetch_parse_node_by_index(x).node;
                right_node = Some(Box::new(tmp_node.clone()));
                error2 = Some(tmp_node.fetch_error_info());
            },
            NodeOrIndex::None => {
                right_node = None;
            }
        }
        ParseNode {
            node: Node::Branch {
                left: left_node,
                right: right_node,
                branch: (branch.to_string(), ErrorInfo::join_error(error1, error2).unwrap()) 
            },
            status: status.to_string(),
            lock: false
        }
    }
    fn swap_assign_and_variable(&mut self, index: usize){
        let node = self.fetch_parse_node_by_index(index);
        self.tmp_assign_nodes.push(node);
        self.tmp_assign_nodes.push(
            ParseNode {
                node: Node::None,
                status: String::from("SEMICOL"),
                lock: false
            }
        );
        if let Node::Branch{left: Some(x), ..} = self.fetch_parse_node_by_index(index).node {
            let tmp_parse_node = ParseNode {
                node: *x,
                status: String::from("VARIABLE"),
                lock: false
            };
            self.set_parase_node_by_index(index, tmp_parse_node.clone());
        }
    }
    fn overwrite_nodes(&mut self, node: ParseNode, from_begin: usize, from_end: usize) {
        let mut tmp_array: Vec<ParseNode> = vec![node];
        let begin = self.begin + from_begin;
        let end = self.end - from_end;
        self.nodes.splice(begin..end-1, tmp_array.iter().cloned());
        self.init();
    }
    fn overwrite_node_status(&mut self, index: usize, status: &str){
        self.nodes[self.begin + index].status = status.to_string();
        self.init();
    }
    fn lock_variable(&mut self) -> bool {
        if self.is_match(vec!["VARIABLE", "EQ"]) && !self.is_locked_node_by_index(0) {
            self.set_lock_node_by_index(0);
        }
        false
    }
    fn lock_if_else(&mut self) -> bool {
        if self.is_match(vec!["BRANCH", "ELSE"]) && !self.is_locked_node_by_index(0) {
            self.set_lock_node_by_index(0);
        }
        false
    }
    fn lock_mulexp(&mut self) -> bool {
        if self.is_match(vec!["EXPRESSION", "ADDORSUB", "MULEXP", "MULTIORDIV"]) && !self.is_locked_node_by_index(2) {
            self.set_lock_node_by_index(2);
        } else if self.is_match(vec!["MULEXP", "MULTIORDIV"]) && !self.is_locked_node_by_index(0) {
            self.set_lock_node_by_index(0);
        }
        false
    }
    fn lock_for(&mut self) -> bool {
        if self.is_match(vec!["FOR", "L_P", "ASSIGN", "SEMICOL"]) && !self.is_locked_node_by_index(2) {
            self.set_lock_node_by_index(2);
        } else if self.is_match(vec!["FOR", "L_P", "FOR_DEFINE", "ASSIGN", "SEMICOL"]) && !self.is_locked_node_by_index(3) {
            self.set_lock_node_by_index(3);
        }
        false
    }
    fn to_function(&mut self) -> bool {
        if self.is_match(vec!["FUNC_DEF", "L_P", "R_P", "L_CB", "SENTENCES", "R_CB"]) {
            let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(4), "function", "FUNCTION");
            self.overwrite_nodes(node, 0, 0);
            true
        }else{
            false
        }
    }
    fn to_func_name(&mut self) -> bool {
        let mut left_node;
        if self.is_match(vec!["TYPE", "VARIABLE", "L_P"]){
            left_node = self.fetch_parse_node_by_index(0).node;
        } else if self.is_match(vec!["VOID", "VARIABLE", "L_P"]){
            left_node = self.fetch_parse_node_by_index(0).node;
            if let Node::StatusNode(x) = left_node {
                left_node = Node::Type(String::from("void"), x);
            }
        } else{
            return false;
        }
        let mut right_node = self.fetch_parse_node_by_index(1).node;
        if let Node::Variable(x, y) = right_node {
            right_node = Node::FuncName(x.clone(), y.clone());
        }
        let node = self.make_node(NodeOrIndex::node(left_node), NodeOrIndex::node(right_node), "func_define", "FUNC_DEF");
        self.overwrite_nodes(node, 0, 1);
        true
    }
    fn to_assign_variable(&mut self) -> bool {
        if self.is_match(vec!["TYPE", "ASSIGN", "SEMICOL"]) {
            self.swap_assign_and_variable(1);
            true
        } else if self.is_match(vec!["DEF_LOOP", "COMMA", "ASSIGN", "COMMA"]) ||
                  self.is_match(vec!["DEF_LOOP", "COMMA", "ASSIGN", "SEMICOL"]) {
            self.swap_assign_and_variable(2);
            true
        } else if self.is_match(vec!["TYPE", "ASSIGN", "COMMA"]) {
            self.swap_assign_and_variable(1);
            true
        } else {
            false
        }
    }
    fn to_define_sentence(&mut self) -> bool {
        if  self.is_match(vec!["TYPE", "DEF_LOOP", "SEMICOL"]) ||
            self.is_match(vec!["TYPE", "VARIABLE", "SEMICOL"]) {
            for i in 0..self.tmp_assign_nodes.len() {
                self.insert_parse_node_by_index(3+i, self.tmp_assign_nodes[i].clone());
            }
            self.tmp_assign_nodes = vec![];
            let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(1), "define", "SENTENCE");
            self.overwrite_nodes(node, 0, 0);
            true
        } else if self.is_match(vec!["FOR", "L_P", "TYPE", "VARIABLE", "SEMICOL"]) {
            for i in 0..self.tmp_assign_nodes.len() {
                self.insert_parse_node_by_index(5+i, self.tmp_assign_nodes[i].clone());
            }
            self.tmp_assign_nodes = vec![];
            let node = self.make_node(NodeOrIndex::index(2), NodeOrIndex::index(3), "define", "FOR_DEFINE");
            self.overwrite_nodes(node, 2, 0);
            true
        } else {
            false
        }
    }
    fn to_recursive_def_loop(&mut self) -> bool {
        if  self.is_match(vec!["DEF_LOOP", "COMMA", "VARIABLE", "COMMA"]) ||
            self.is_match(vec!["DEF_LOOP", "COMMA", "VARIABLE", "SEMICOL"]) {
                let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(2), "", "DEF_LOOP");
                self.overwrite_nodes(node, 0, 1);
                true
        } else {
            false
        }
    }
    fn to_def_loop(&mut self) -> bool {
        if self.is_match(vec!["TYPE", "VARIABLE", "COMMA"]) {
            self.overwrite_node_status(1, "DEF_LOOP");
            true
        } else {
            false
        }
    }
    fn to_assign(&mut self) -> bool {
        if self.is_match(vec!["VARIABLE", "EQ", "EXPRESSION", "SEMICOL"]) ||
           self.is_match(vec!["VARIABLE", "EQ", "EXPRESSION", "R_P"]) ||
           self.is_match(vec!["VARIABLE", "EQ", "EXPRESSION", "COMMA"]){
            let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(2), "=", "ASSIGN");
            self.overwrite_nodes(node, 0, 1);
            true
        } else {
            false
        }
    }
    fn to_if(&mut self) -> bool {
        if self.is_match(vec!["IF", "L_P", "COMPARE", "R_P", "L_CB", "SENTENCES", "R_CB"]) {
            let node = self.make_node(NodeOrIndex::index(2), NodeOrIndex::index(5), "if", "BRANCH");
            self.overwrite_nodes(node, 0, 0);
            true
        }else if self.is_match(vec!["IF", "L_P", "COMPARE", "R_P", "SENTENCE"]) {
            let node = self.make_node(NodeOrIndex::index(2), NodeOrIndex::index(4), "if", "BRANCH");
            self.overwrite_nodes(node, 0, 0);
            true
        } else {
            false
        }
    }
    fn to_if_else(&mut self) -> bool {
        let right_index: usize;
        if self.is_match(vec!["BRANCH", "ELSE", "L_CB", "SENTENCES", "R_CB"]) {
            right_index = 3;
        }else if self.is_match(vec!["BRANCH", "ELSE", "SENTENCE"]) {
            right_index = 2;
        }else {
            return false;
        }
        let right_left_node = self.make_node(NodeOrIndex::index(right_index), NodeOrIndex::None, "", "");
        let right_node = self.make_node(NodeOrIndex::node(right_left_node.node), NodeOrIndex::None, "else", "");
        let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::node(right_node.node), "", "BRANCH");
        self.overwrite_nodes(node, 0, 0);
        true
    }
    fn to_for(&mut self) -> bool { 
        let mut node_left:  ParseNode;
        let mut node_right: ParseNode;
        if self.is_match(vec!["FOR", "L_P", "ASSIGN", "SEMICOL", "COMPARE", "SEMICOL", "ASSIGN", "R_P",
                              "L_CB", "SENTENCES", "R_CB"]) {
            node_left = self.make_node(NodeOrIndex::index(2), NodeOrIndex::None, "", "");
            let node_right_left = self.make_node(NodeOrIndex::index(4), NodeOrIndex::None, "", "");
            let node_right_right = self.make_node(NodeOrIndex::index(9), NodeOrIndex::index(6), "", "");
            node_right = self.make_node(NodeOrIndex::node(node_right_left.node), NodeOrIndex::node(node_right_right.node), "", "");
        }else if self.is_match(vec!["FOR", "L_P", "ASSIGN", "SEMICOL", "COMPARE", "SEMICOL", "ASSIGN", "R_P", "SENTENCE"]) {
            node_left = self.make_node(NodeOrIndex::index(2), NodeOrIndex::None, "", "");
            let node_right_left = self.make_node(NodeOrIndex::index(4), NodeOrIndex::None, "", "");
            let node_right_right = self.make_node(NodeOrIndex::index(8), NodeOrIndex::index(6), "", "");
            node_right = self.make_node(NodeOrIndex::node(node_right_left.node), NodeOrIndex::node(node_right_right.node), "", "");
        }else if self.is_match(vec!["FOR", "L_P", "FOR_DEFINE", "ASSIGN", "SEMICOL", "COMPARE", 
                                    "SEMICOL", "ASSIGN", "R_P", "L_CB", "SENTENCES", "R_CB"]) {
            let node_left_right = self.make_node(NodeOrIndex::index(3), NodeOrIndex::None, "", "");
            node_left = self.make_node(NodeOrIndex::index(2), NodeOrIndex::node(node_left_right.node), "", "");
            let node_right_left = self.make_node(NodeOrIndex::index(5), NodeOrIndex::None, "", "");
            let node_right_right = self.make_node(NodeOrIndex::index(10), NodeOrIndex::index(7), "", "");
            node_right = self.make_node(NodeOrIndex::node(node_right_left.node), NodeOrIndex::node(node_right_right.node), "", "");
        }else{
            return false;
        }
        let parse_node = self.make_node(NodeOrIndex::node(node_left.node), NodeOrIndex::node(node_right.node), "for", "LOOP");
        self.overwrite_nodes(parse_node, 0, 0);
        true
    }
    fn to_while(&mut self) -> bool {
        if self.is_match(vec!["WHILE", "L_P", "COMPARE", "R_P", "L_CB", "SENTENCES", "R_CB"]) {
            let node = self.make_node(NodeOrIndex::index(2), NodeOrIndex::index(5), "while", "LOOP");
            self.overwrite_nodes(node, 0, 0);
            true
        }else if self.is_match(vec!["WHILE", "L_P", "COMPARE", "R_P", "SENTENCE"]) {
            let node = self.make_node(NodeOrIndex::index(2), NodeOrIndex::index(4), "while", "LOOP");
            self.overwrite_nodes(node, 0, 0);
            true
        } else {
            false
        }
    }
    fn to_expression(&mut self) -> bool {
        if self.is_match(vec!["EXPRESSION", "ADDORSUB", "MULEXP"]) && !self.is_locked_node_by_index(2) {
            if let Node::Branch{branch: (x, _), ..} = self.fetch_parse_node_by_index(1).node {
                let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(2), &*x, "EXPRESSION");
                self.overwrite_nodes(node, 0, 0);
            }
            true
        } else if self.is_match(vec!["MULEXP"]) && !self.is_locked_node_by_index(0) {
            self.overwrite_node_status(0, "EXPRESSION");
            true
        } else {
            false
        }
    }
    fn to_mulexp(&mut self) -> bool {
        if self.is_match(vec!["MULEXP", "MULTIORDIV", "PRIMARY"]){
            if let Node::Branch{branch: (x, _), ..} = self.fetch_parse_node_by_index(1).node {
                let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(2), &*x, "MULEXP");
                self.overwrite_nodes(node, 0, 0);
            }
            true
        }else if self.is_match(vec!["PRIMARY"]){
            self.overwrite_node_status(0, "MULEXP");
            true
        } else {
            false
        }
    }
    fn to_primary(&mut self) -> bool {
        if self.is_match(vec!["L_P", "EXPRESSION", "R_P"]) {
            let node = self.fetch_parse_node_by_index(1).node;
            self.overwrite_nodes(ParseNode {
                node:  node,
                status: String::from("PRIMARY"),
                lock: false
            }, 0, 0);
            true
        }else if self.is_match(vec!["FIGURE"]) ||
                 (self.is_match(vec!["VARIABLE"]) && !self.is_locked_node_by_index(0)) {
            self.overwrite_node_status(0, "PRIMARY");
            true
        } else {
            false
        }
    }
    fn to_compare(&mut self) -> bool {
        if self.is_match(vec!["EXPRESSION", "COMPARISON", "EXPRESSION"]) {
            if let Node::Branch{branch: (x, _), ..} = self.fetch_parse_node_by_index(1).node {
                let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(2), &*x, "COMPARE");
                self.overwrite_nodes(node, 0, 0);
            }
            true
        } else {
            false
        }
    }
    fn to_return_sentence(&mut self) -> bool {
        if self.is_match(vec!["RETURN", "EXPRESSION", "SEMICOL"]) {
            let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(1), "return", "SENTENCE");
            self.overwrite_nodes(node, 0, 0);
            true
        } else if self.is_match(vec!["RETURN", "SEMICOL"]) {
            let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::None, "void_return", "SENTENCE");
            self.overwrite_nodes(node, 0, 0);
            true
        } else {
            false
        }
    }
    fn to_recursive_sentences(&mut self) -> bool {
        if self.is_match(vec!["SENTENCES", "SENTENCE"]) {
            let node = self.make_node(NodeOrIndex::index(0), NodeOrIndex::index(1), "", "SENTENCES");
            self.overwrite_nodes(node, 0, 0);
            true
        } else if self.is_match(vec!["SENTENCE"]) {
            self.overwrite_node_status(0, "SENTENCES");
            true
        } else {
            false
        }
    }
    fn to_sentence(&mut self) -> bool {
        if self.is_match(vec!["LOOP"]) ||
           (self.is_match(vec!["BRANCH"]) && !self.is_locked_node_by_index(0)) ||
           (self.is_match(vec!["ASSIGN", "SEMICOL"]) && !self.is_locked_node_by_index(0)) {
            let node = self.fetch_parse_node_by_index(0).node;
            self.overwrite_nodes(ParseNode {
                node: node,
                status: String::from("SENTENCE"),
                lock: false
            }, 0, 0);
            true
        } else {
            false
        }
    }
}


pub fn parse(lexical_array: Vec<(Token, ErrorInfo)>) -> Result<Node, ErrorInfo> {
    let mut nodes:ParseNodes = ParseNodes::new();
    
    for word in lexical_array {
        nodes.array_push(ParseNode::new(word));
    }

    nodes.init();

    loop {

        // // Code for debug
            // print!("[");
            // for i in (0..nodes.nodes.len()) {
            //     print!("{:?},", nodes.nodes[i].status);
            // }
            // println!("]");
        
        if
            nodes.lock_variable()           ||
            nodes.lock_if_else()            ||
            nodes.lock_mulexp()             ||
            nodes.lock_for()                ||
            nodes.to_function()             ||
            nodes.to_func_name()            ||
            nodes.to_assign_variable()      ||
            nodes.to_define_sentence()      ||
            nodes.to_recursive_def_loop()   ||
            nodes.to_def_loop()             ||
            nodes.to_assign()               ||
            nodes.to_if()                   ||
            nodes.to_if_else()              ||
            nodes.to_for()                  ||
            nodes.to_while()                ||
            nodes.to_expression()           ||
            nodes.to_mulexp()               ||
            nodes.to_primary()              ||
            nodes.to_compare()              ||
            nodes.to_return_sentence()      ||
            nodes.to_recursive_sentences()  ||
            nodes.to_sentence()
        {
            nodes.init();
        }else{
            nodes.increment();
        }

        if let None = nodes.next(){
            break;
        }
    }

    if nodes.nodes[0].status == "FUNCTION" {
        Ok(nodes.nodes[0].node.clone())
    } else {
        let mut error = ErrorInfo::new(0, 0);
        error.set_message(String::from("Syntax Error"));
        Err(error)
    }
}


