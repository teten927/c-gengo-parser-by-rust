use std::fs;
use std::error::Error;
use std::io::{self, Write};


const INPUT_FILE: &str = "input/main.c";
const LOG_FILE: &str = "output/lex_parse_result.log";
const OUTPUT_FILE: &str = "output/main.s";

mod lex;
mod parse;
mod make_code;
mod error_handler;

fn output_parse(mut output: &mut String, node: parse::Node, mut index: u32, message: String) -> u32 {
    let mut new_index = index+1;
    match node {
        parse::Node::Branch{left:x, right:y, branch:z} => {
            let mut tmp: String;
            *output += &*format!("[{:?} : {:?}]\t\t=> {:?}\n", index, message, z);
            if let Some(xx) = x {
                new_index = output_parse(&mut output, *xx, new_index, format!("left in {}", index));
            }
            if let Some(yy) = y {
                new_index = output_parse(&mut output, *yy, new_index, format!("right in {}", index));
            }
        },
        x @ _ => {
            *output += &*format!("[{:?} : {:?}]\t\t=> {:?}\n", index, message, x);
        }
    }
    new_index
}

fn main() -> Result<(), Box<std::error::Error>> {
    let mut content = fs::read_to_string(INPUT_FILE)?;
    let mut log_file =  fs::File::create(LOG_FILE)?;
    let mut log = String::new();

    let mut lex_array = lex::lex(&content).unwrap_or_else(|x| {println!("{}", x.create_error(content.clone())); std::process::exit(0); });
    log += "----------Result(Lexical Analysys)----------\n";
    for x in &lex_array {
        log += &*format!("{:?}\n", x.0);
    }
    log += "--------------------------------------------\n";
    
    let parse_tree = parse::parse(lex_array).unwrap_or_else(|x| {println!("{}", x.create_error(content.clone())); std::process::exit(0); });
    log += "----------Result(Parse Analysys)----------\n";
    output_parse(&mut log, parse_tree.clone(), 0, "root == 0".to_string());
    log += "------------------------------------------\n";
    write!(log_file, "{}", log)?;
    log_file.flush()?;

    let output = make_code::make_code(parse_tree.clone()).unwrap_or_else(|x| {println!("{}", x.create_error(content.clone())); std::process::exit(0); });
    let mut output_file =  fs::File::create(OUTPUT_FILE)?;
    write!(output_file, "{}", output)?;
    output_file.flush()?;
  
    println!("\nCompile Success!!");
    Ok(())
}