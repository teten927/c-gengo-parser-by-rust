use std::fs;
use std::io::{self, Write};
use std::error::Error;
use std::collections::HashMap;
use std::collections::VecDeque;
use parse::Node;
use error_handler::ErrorInfo;

#[derive (Debug, Clone)]
pub enum NodeComponent {
    Number(i32, ErrorInfo),
    Variable(String, ErrorInfo),
    TmpRegister(String)
}

#[derive (Debug, Clone)]
pub enum AssemblyStorage {
    Register(String),
    RelativeAddress(usize),
    None
}

#[derive (Debug, Clone)]
pub struct Function {
    return_type: String,
    func_name: String
}

impl Function {
    fn new() -> Function {
        Function {
            return_type: String::new(),
            func_name: String::new()
        }
    }
}

#[derive (Debug, Clone)]
pub struct VariableInfo {
    _type: String,
    size: usize,
    location: AssemblyStorage
}

#[derive (Debug, Clone)]
pub struct CodeMaker {
    variable_keys: Vec<Vec<String>>,
    variables: HashMap<String, VariableInfo>,
    unused_register_que: VecDeque<String>,
    used_register_que: VecDeque<String>,
    register_variable_map: HashMap<String, String>,
    tmp_register_status: HashMap<String, bool>,
    will_release_tmp_register: Vec<String>,
    node_component_stack: Vec<NodeComponent>,
    now_function: Function,
    if_defined: bool,
    defined_variables: ((String, ErrorInfo), Vec<(String, ErrorInfo)>),
    type_size_map: HashMap<String, usize>,
    start_address: usize,
    address_table: Vec<bool>,
    label_count: usize,
    label_stack: Vec<String>,
    text_code_string: String,
    tmp_text_code_string: String,
    data_code_string: String
}

impl CodeMaker {
    fn new() -> CodeMaker {
        CodeMaker {
            variable_keys: Vec::new(),
            variables: HashMap::new(),
            unused_register_que: VecDeque::new(),
            used_register_que: VecDeque::new(),
            register_variable_map: HashMap::new(),
            tmp_register_status: HashMap::new(),
            will_release_tmp_register: Vec::new(),
            node_component_stack: Vec::new(),
            now_function: Function::new(),
            if_defined: false,
            defined_variables: ((String::new(), ErrorInfo::new(0, 0)), Vec::new()),
            type_size_map: HashMap::new(),
            start_address: 4,
            address_table: Vec::new(),
            label_count: 1,
            label_stack: Vec::new(),
            text_code_string: String::from("\t\t.text\n\t\t.align 2\n"),
            tmp_text_code_string: String::new(),
            data_code_string: String::from("\t\t.data\n\t\t.align 2\n")
        }
    }
    fn explore_node(&mut self, node: Node) -> Result<(), ErrorInfo> {
        match node {
            Node::Branch{left:_left, right:_right, branch:_branch} => {
                if let (b, _) = _branch.clone() {
                    self.analyze_branch_enter(b);
                }
                if let Some(l) = _left {
                    self.explore_node(*l)?
                }
                if let (b, _) = _branch.clone() {
                    self.analyze_branch_center(b);
                }
                if let Some(r) = _right {
                    self.explore_node(*r)?
                }
                if let (b, err) = _branch.clone() {
                    self.analyze_branch_leave(b, err)?
                }
            },
            x @ _ => {
                self.analyze_leaf(x)?
            }
        }
        return Ok(());
    }
    fn analyze_branch_enter(&mut self, branch: String) {
        match &*branch {
            x @ "function" |
            x @ "if"       |
            x @ "else"     |
            x @ "for"      |
            x @ "while"    =>   {
                let keys: Vec<String> = self.variables.iter().map(|(s, _)| s.clone()).collect();
                self.variable_keys.push(keys);
                
                if x == "function" { return; }

                let label = String::from("label") + &*self.label_count.to_string();
                self.label_stack.push(label.clone());
                self.label_count += 1;

                if x == "else" {
                    let mut split_str: Vec<&str> = self.tmp_text_code_string.split('\n').collect();
                    let tmp_string = format!("\t\tj {}", label);
                    split_str.insert(split_str.len() -2, &tmp_string);
                    self.tmp_text_code_string = split_str.join("\n");
                }

                if x != "while" { return; }

                let loop_label = String::from("loop_") + &label;
                self.tmp_text_code_string += &*format!("{}:\n", loop_label);
            },
            "define" | "func_define"    =>  {
                self.if_defined = true;
            },
            _   =>  {}
        }
    }
    fn analyze_branch_center(&mut self, branch: String) {
        match &*branch {
            "for" =>   {
                let label = String::from("loop_") + self.label_stack.last().unwrap();
                self.tmp_text_code_string += &*format!("{}:\n", label);
            },
            _   => {}
        }

    }
    fn analyze_branch_leave(&mut self, branch: String, error_info: ErrorInfo) -> Result<(), ErrorInfo> {
        match &*branch {
            x @ "function" |
            x @ "if"       |
            x @ "else"     |
            x @ "for"      |
            x @ "while"    =>   {

                
                let remain_keys = self.variable_keys.pop().unwrap().clone();
                let variable_keys: Vec<String> = self.variables.keys().map(|k| k.clone()).collect();
                for key in variable_keys {
                    if !remain_keys.contains(&key) {
                        self.variables.remove(&key);
                    }
                }

                if x == "function" {
                    self.text_code_string += &*format!("{}:\n", self.now_function.func_name);
                    self.text_code_string += &*format!("\t\taddi $sp $sp -{}\n", self.start_address + self.address_table.len()*4);
                    self.text_code_string += &*format!("\t\tsw $ra 0($sp)\n");
                    self.text_code_string += &self.tmp_text_code_string;
                    let func_label = self.now_function.func_name.clone() + "_end";
                    self.text_code_string += &*format!("{}:\n", func_label);
                    self.text_code_string += &*format!("\t\tlw $ra 0($sp)\n");
                    self.text_code_string += &*format!("\t\taddi $sp $sp {}\n", self.start_address + self.address_table.len()*4);
                    self.text_code_string += &*format!("\t\tj $ra\n");
                    self.now_function = Function::new();
                    self.tmp_text_code_string = String::new();
                    return Ok(());
                }

                let label = self.label_stack.pop().unwrap();
                let loop_label = String::from("loop_") + &label;
                if x == "for" || x == "while" {
                    self.tmp_text_code_string += &*format!("\t\tj {}\n", loop_label);
                }
                self.tmp_text_code_string += &*format!("{}:\n", label);

                return Ok(());
            },
            "func_define"   =>  {
                self.if_defined = false;
                self.defined_variables = ((String::new(), ErrorInfo::new(0,0)), Vec::new());        
                return Ok(());
            },
            "define"    =>  {
                self.handle_define()?;
                self.if_defined = false;
                self.defined_variables = ((String::new(), ErrorInfo::new(0,0)), Vec::new());        
                return Ok(());
            },
            "+" | "-" | "*" | "/"   =>  { self.handle_operator(&branch)?; },
            "=" => { self.handle_assign()?; },
            "<" | ">" | "<=" | ">=" | "==" | "!="   =>  { self.handle_comparation(&branch)?; },
            "return"    => { self.handle_return(false, error_info)?; },
            "void_return"   => { self.handle_return(true, error_info)?; },
            _   =>  {}
        }

        Ok(())

    }
    fn analyze_leaf(&mut self, node: Node) -> Result<(), ErrorInfo> {
        match node {
            Node::Number(x, err) => {
                self.node_component_stack.push(NodeComponent::Number(x, err));
            },
            Node::Variable(x, mut err) => {
                if self.if_defined {
                    if !self.variables.contains_key(&x) {
                        self.defined_variables.1.push((x, err));
                    } else {
                        err.set_message(String::from("Duplicate declarations within the same scope"));
                        return Err(err);
                    }
                } else {
                    if self.variables.contains_key(&x) {
                        self.node_component_stack.push(NodeComponent::Variable(x, err));
                    } else {
                        err.set_message(String::from("Undefined variable"));
                        return Err(err);
                    }
                }
            },
            Node::Type(x, err) => {
                self.defined_variables.0 = (x, err);
            },
            Node::FuncName(x, _) => {
                if let (_type, err) = self.defined_variables.0.clone() {
                    self.now_function = Function {
                        return_type: _type,
                        func_name: x
                    }
                }
            },
            _ => {}
        }
        return Ok(());
    }
    fn handle_define(&mut self) -> Result<(), ErrorInfo> {
        if let (variable_type, _) = self.defined_variables.0.clone() {
            for (x, mut err) in self.defined_variables.1.clone() {
                let variable = VariableInfo {
                    _type: variable_type.clone(),
                    size: self.type_size_map.get(&variable_type).unwrap().clone(),
                    location: AssemblyStorage::None
                };
                if let Some(_) = self.variables.insert(x, variable) {
                    err.set_message(String::from("Duplicate declarations within the same scope"));
                    return Err(err);
                }
            }
        }   
        return Ok(());
    }
    fn handle_assign(&mut self) -> Result<(), ErrorInfo> {
        let mut operator_param = String::new();
        let right_component = self.node_component_stack.pop().unwrap();
        let left_component = self.node_component_stack.pop().unwrap();

        let mut param1 = self.analyze_assigned_component(&left_component)?;
        let mut param2 = String::new();
        if let NodeComponent::Number(x, _) = (&right_component) {
            operator_param = String::from("li");
            param2 = (*x).to_string();
        } else {
            operator_param = String::from("move");
            param2 = self.analyze_node_component_register(&right_component)?;
        }

        self.release_tmp_register();
        self.tmp_text_code_string += &*format!("\t\t{} {} {}\n", operator_param, param1, param2);
        Ok(())
    }
    fn handle_operator(&mut self, operator_code: &String) -> Result<(), ErrorInfo> {
        let mut operator_param = String::new();
        let right_component = self.node_component_stack.pop().unwrap();
        let left_component = self.node_component_stack.pop().unwrap();

        match &*(*operator_code) {
            "+" => operator_param = String::from("add"),
            "-" => operator_param = String::from("sub"),
            "*" => operator_param = String::from("mul"),
            "/" => operator_param = String::from("div"),
            _   =>  {}
        }

        if let (NodeComponent::Number(x, err1),NodeComponent::Number(y, err2)) = (&left_component, &right_component) {
            if let Some(value) = self.calculate_num(operator_code, *x, *y) {
                let error = ErrorInfo::new(err1.get_start(), err2.get_start());
                self.node_component_stack.push(NodeComponent::Number(value, error));
                return Ok(());   
            }   else {
                let mut error = ErrorInfo::new(err1.get_start(), err2.get_end());
                error.set_message(String::from("Not allowed operation"));
                return Err(error);
            }
        }

        let mut param1 = self.analyze_node_component_register(&left_component)?;
        let mut param2 = self.analyze_node_component_register(&right_component)?;
       
        match (&left_component, &right_component) {
            (NodeComponent::Number(x, _), _) => {
                let mut tmp_param = String::new();
                if operator_code == "+" {
                    operator_param = String::from("addi");
                    param1 = param2;
                    param2 = (*x).to_string();
                } else if operator_code == "-" {
                    operator_param = String::from("addi");
                    param1 = param2;
                    param2 = String::from("-") + &*((*x).to_string());
                }
            },
            (_, NodeComponent::Number(x, _)) => {
                if operator_code == "+" {
                    operator_param = String::from("addi");
                    param2 = (*x).to_string();
                } else if operator_code == "-" {
                    operator_param = String::from("addi");
                    param2 = String::from("-") + &*((*x).to_string());
                }
            },
            _ => {}
        }

        self.release_tmp_register();
        match self.get_tmp_register() {
            Ok(tmp_register) => {
                self.tmp_text_code_string += &*format!("\t\t{} {} {} {}\n", operator_param, tmp_register, param1, param2);
                self.node_component_stack.push(NodeComponent::TmpRegister(tmp_register));
                Ok(())
            },
            Err(err) => {
                Err(err)
            }
        }
    }
    fn calculate_num(&mut self, operator_code: &String, x: i32, y: i32) -> Option<i32> {
        match &*(*operator_code) {
            "+" => Some(x + y),
            "-" => Some(x - y),
            "*" => Some(x * y),
            "/" => Some(x / y),
            _   => None
        }
    }
    fn handle_comparation(&mut self, comparison_code: &String) -> Result<(), ErrorInfo> {
        let mut operator_param = String::new();

        match &*(*comparison_code) {
            ">" => operator_param = String::from("ble"),
            "<" => operator_param = String::from("bge"),
            ">=" => operator_param = String::from("blt"),
            "<=" => operator_param = String::from("bgt"),
            "==" => operator_param = String::from("bne"),
            "!=" => operator_param = String::from("beq"),
            _   =>  {}
        }

        let right_component = self.node_component_stack.pop().unwrap();
        let left_component = self.node_component_stack.pop().unwrap();
        let mut param1 = self.analyze_node_component_register(&left_component)?;
        let mut param2 = self.analyze_node_component_register(&right_component)?;

        self.release_tmp_register();

        let label = self.label_stack.last().unwrap();
        self.tmp_text_code_string += &*format!("\t\t{} {} {} {}\n", operator_param, param1, param2, *label);

        Ok(())
    }
    fn handle_return(&mut self, is_void: bool, mut error: ErrorInfo) -> Result<(), ErrorInfo> {
        if (is_void && &self.now_function.return_type == "void") {
            let func_label = self.now_function.func_name.clone() + "_end";
            self.tmp_text_code_string += &*format!("\t\tj {}\n", func_label);
            Ok(())
        } else if (!is_void && &self.now_function.return_type != "void") {
            let left_component = self.node_component_stack.pop().unwrap();
            let param = self.analyze_node_component_register(&left_component)?;
            self.release_tmp_register();
            self.tmp_text_code_string += &*format!("\t\tmove $v1 {}\n", param);
            let func_label = self.now_function.func_name.clone() + "_end";
            self.tmp_text_code_string += &*format!("\t\tj {}\n", func_label);
            Ok(())
        } else {
            error.set_message(String::from("Return type mismatch"));
            Err(error)
        }
    }
    fn analyze_assigned_component(&mut self, node_component: &NodeComponent) -> Result<String, ErrorInfo> {
        if let NodeComponent::Variable(x, err) = node_component {
            match self.handle_register_location((*x).clone(), true, (*err).clone()) {
                Ok(register_name) => {
                    Ok(register_name.clone())
                },
                Err(err) => {
                    Err(err)
                }
            }
        } else {Err(ErrorInfo::new(0,0))}
    }
    fn analyze_node_component_register(&mut self, node_component: &NodeComponent) -> Result<String, ErrorInfo> {
        match node_component {
            NodeComponent::Number(x, _) => {
                match self.get_tmp_register() {
                    Ok(tmp_register) => {
                        self.tmp_text_code_string += &*format!("\t\tli {} {}\n", tmp_register.clone(), (*x).to_string());
                        self.will_release_tmp_register.push(tmp_register.clone());
                        Ok(tmp_register)
                    },
                    Err(err) => {
                        Err(err)
                    }
                }
            },
            NodeComponent::Variable(x, err) => {
                match self.handle_register_location((*x).clone(), false, (*err).clone()) {
                    Ok(register_name) => {
                        Ok(register_name)
                    },
                    Err(err) => {
                        Err(err)
                    }
                }
            },
            NodeComponent::TmpRegister(x) => {
                self.will_release_tmp_register.push((*x).clone());
                Ok((*x).clone())
            }
        }
    }
    fn handle_register_location(&mut self, variable_name: String, is_allow_none: bool, mut error: ErrorInfo) -> Result<String,ErrorInfo> {
        let allocated_variable = self.variables.get(&variable_name).unwrap().clone();
        let register_name: String;
        match &allocated_variable.location {
            AssemblyStorage::Register(y) =>  {
                self.used_register_que.retain(|z| (*z)!=(*y));
                self.used_register_que.push_front(y.clone());
                return Ok(y.clone());
            },
            AssemblyStorage::RelativeAddress(x) =>  {
                register_name = self.get_register_location();
                self.tmp_text_code_string += &*format!("\t\tlw {} {}($sp)\n", register_name, x.to_string());
                self.remove_memory_address(*x, allocated_variable.size);     
            },
            AssemblyStorage::None if is_allow_none => {
                register_name = self.get_register_location();
            },
            _ => {
                error.set_message(String::from("Unassigned variable"));
                return Err(error);
            }
        }
        let mut new_variable = self.variables.get_mut(&variable_name).unwrap();
        new_variable.location = AssemblyStorage::Register(register_name.clone());
        self.register_variable_map.insert(register_name.clone(), variable_name.clone());
        Ok(register_name)
    }
    fn get_register_location(&mut self) -> String {
        let mut register_name: String;
        if self.unused_register_que.is_empty() {
            register_name = self.used_register_que.pop_back().unwrap();
            self.used_register_que.push_front(register_name.clone()); 
            let old_variable_name = self.register_variable_map.get(&register_name).unwrap().clone();
            let old_variable = self.variables.get(&old_variable_name).unwrap().clone();
            let memory_address = self.get_memory_address(old_variable.size);
            let mut writed_old_variable = self.variables.get_mut(&old_variable_name).unwrap();
            writed_old_variable.location = AssemblyStorage::RelativeAddress(memory_address.clone());
            self.tmp_text_code_string += &*format!("\t\tsw {} {}($sp)\n", register_name, memory_address.to_string());
        } else {
            register_name = self.unused_register_que.pop_front().unwrap();
            self.used_register_que.push_front(register_name.clone());
        }
        register_name
    }
    fn get_memory_address(&mut self, size: usize) -> usize {
        let size_per_32bit = (size+3)/4;
        let mut tmp_size = size_per_32bit;
        for i in 0..self.address_table.len() {
            if !self.address_table[i] {
                tmp_size -= 1;
            } else {
                tmp_size = size_per_32bit;
            }
            if tmp_size > 0 {continue;}
            let start = i-(size_per_32bit-1);
            for j in start..(i+1) {
                self.address_table[j] = true;
            }
            return self.start_address + 4*start;
        }
        for i in 0..tmp_size {
            self.address_table.push(true);
        }
        return self.start_address + 4*(self.address_table.len()-size_per_32bit);
    }
    fn remove_memory_address(&mut self, i: usize, size: usize) {
        let start = (i-self.start_address)/4;
        let end = start + (size + 3)/4;
        for j in start..end {
            self.address_table[j] = false;
        }
    }
    fn get_tmp_register(&mut self) -> Result<String, ErrorInfo> {
        for (k, v) in self.tmp_register_status.iter_mut() {
            if *v == false {
                *v = true;
                return Ok((*k).clone())
            }
        }
        let mut error = ErrorInfo::new(0,0);
        error.set_message(String::from("No free space in tmp register"));
        return Err(error)
    }
    fn release_tmp_register(&mut self){
        for k in &self.will_release_tmp_register {
            self.tmp_register_status.insert((*k).clone(), false);
        }
        self.will_release_tmp_register = Vec::new();
    }
}

pub fn make_code(node: Node) -> Result<String, ErrorInfo> {
    let registers_for_variable = vec![
                    // All Register for variable
                     String::from("$s0"),
                     String::from("$s1"),
                     String::from("$s2"),
                     String::from("$s3"),
                     String::from("$s4"),
                     String::from("$s5"),
                     String::from("$s6"),
                     String::from("$s7")
                    ];
    let registers_for_tmp = vec![
                    // All Register for tmp variable
                     String::from("$t0"),
                     String::from("$t1"),
                     String::from("$t2"),
                     String::from("$t3"),
                     String::from("$t4"),
                     String::from("$t5"),
                     String::from("$t6"),
                     String::from("$t7")
                    ];
    let type_size_map = [
                    (String::from("int"), 4),
                    (String::from("char"), 1),
                    (String::from("void"), 0)
                    ]
            .iter().cloned().collect();
                

    let mut code_maker = CodeMaker::new();
    code_maker = CodeMaker {
        unused_register_que: VecDeque::from(registers_for_variable),
        tmp_register_status: registers_for_tmp.iter().map(|k| ((*k).clone(), false)).collect(),
        type_size_map: type_size_map,
        ..code_maker
    };
    
    code_maker.explore_node(node)?;

    let output_string = code_maker.text_code_string + "\n" + &code_maker.data_code_string;
    
    Ok((output_string))

}


