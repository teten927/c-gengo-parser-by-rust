extern crate nom;

use lex::nom::{
    Err,
    IResult,
    error::ErrorKind,
    bytes::complete::{tag, take_until},
    character::complete::{multispace0, digit1, alpha1, alphanumeric0, one_of},
    branch::alt,
    combinator::rest_len
};
use error_handler::ErrorInfo;

#[derive (Debug)]
pub enum Token {
    Variable(String),
    Figure(String),
    Arithmetic(String),
    Comparison(String),
    Type(String),
    VOID,
    WHILE,
    FOR,
    IF,
    ELSE,
    EQ,
    L_P,
    R_P,
    L_CB,
    R_CB,
    L_SB,
    R_SB,
    COMMA,
    SEMICOL,
    RETURN,
    None
}

fn ignore_space(input: &str) -> IResult<&str, &str> {
    let (input, _) = multispace0(input)?;
    Ok((input, ""))
}

fn ignore_comment(input: &str) -> IResult<&str, &str> {
    let (input, first) = alt((tag("/*"), tag("//")))(input)?;
    if first == "/*" {
        let (input, _) = take_until("*/")(input)?;
        let (input, _) = tag("*/")(input)?;
        Ok((input, ""))
    }else{
        let (input, _) = take_until("\n")(input)?;
        let (input, _) = tag("\n")(input)?;
        Ok((input, ""))
    }
}

fn get_word(input: &str) -> IResult<&str, Token> {
    let token:Token;
    let (input, first) = alt((tag("_"), alpha1))(input)?;
    let (input, after) = alphanumeric0(input)?;
    let word:&str = &(first.to_string() + after);
    match word {
        "int" => token = Token::Type(String::from("int")),
        "char" => token = Token::Type(String::from("char")),
        "void" => token = Token::VOID,
        "while" => token = Token::WHILE,
        "for" => token = Token::FOR,
        "if" => token = Token::IF,
        "else" => token = Token::ELSE,
        "return" => token = Token::RETURN,
        _ => token = Token::Variable(word.to_string())
    }
    Ok((input, token))
}

fn get_figure(input: &str) -> IResult<&str, Token> {
    let (input, figure) = digit1(input)?;
    Ok((input, Token::Figure(figure.to_string())))
}

fn get_comparison(input: &str) -> IResult<&str, Token> {
    let (input, comparison) = alt((tag("=="), tag("!="), tag(">"), tag("<"), tag(">="), tag("<=")))(input)?;
    Ok((input, Token::Comparison(comparison.to_string())))
}

fn get_arithmetic(input: &str) -> IResult<&str, Token> {
    let token:Token;
    let (input, arithmetic) = one_of::<_, _, (&str, ErrorKind)>("+-*/%")(input)?;
    Ok((input, Token::Arithmetic(arithmetic.to_string())))
}

fn get_other(input: &str) -> IResult<&str, Token> {
    let token:Token;
    let (input, c) = one_of::<_, _, (&str, ErrorKind)>("=(){}[];,")(input)?;
    match &c {
        &'=' => token = Token::EQ,
        &'(' => token = Token::L_P,
        &')' => token = Token::R_P,
        &'{' => token = Token::L_CB,
        &'}' => token = Token::R_CB,
        &'[' => token = Token::L_SB,
        &']' => token = Token::R_SB,
        &';' => token = Token::SEMICOL,
        &',' => token = Token::COMMA,
        _ => token = Token::None
    }
    Ok((input, token))
}

pub fn lex(mut text: &str) -> Result<Vec<(Token, ErrorInfo)>, ErrorInfo> {
    let mut tokens:Vec<(Token, ErrorInfo)> = Vec::new();
    let input_len = text.len();

    loop {
        if let Ok((s, _)) = ignore_space(text) {
            text = s;
        }
        if let Ok((s, _)) = ignore_comment(text) {
            text = s;
            continue;
        }

        let tmp_token: Token;
        let start = input_len - rest_len::<_,(_, ErrorKind)>(text).unwrap().1;

        if let Ok((s, token)) = get_word(text) {
            text = s;
            tmp_token = token;
        } 
        else if let Ok((s, token)) = get_figure(text) {
            text = s;
            tmp_token = token;
        }
        else if let Ok((s, token)) = get_comparison(text) {
            text = s;
            tmp_token = token;
        }
        else if let Ok((s, token)) = get_arithmetic(text) {
            text = s;
            tmp_token = token;
        }
        else if let Ok((s, token)) = get_other(text) {
            text = s;
            tmp_token = token;
        }
        else if text == "" {
            break;
        }
        else {
            let mut error = ErrorInfo::new(start, start +1);
            error.set_message(String::from("Undefined lexical"));
            return Err(error);
        }

        let end = input_len - rest_len::<_,(_, ErrorKind)>(text).unwrap().1;
        let error = ErrorInfo::new(start, end);
        tokens.push((tmp_token, error));
    }
    Ok(tokens)
}