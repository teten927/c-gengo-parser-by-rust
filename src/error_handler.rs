#[derive (Debug, Clone)]
pub struct ErrorInfo {   
    start: usize,
    end: usize,
    message: String
}

impl ErrorInfo {
    pub fn new(start: usize, end: usize) -> ErrorInfo {
        ErrorInfo {
            start: start,
            end: end,
            message: String::new()
        }
    }
    pub fn get_start(&self) -> usize {
        self.start
    }
    pub fn get_end(&self) -> usize {
        self.end
    }
    pub fn set_message(&mut self, message: String) {
        self.message = message;
    }
    pub fn join_error(error_opt1: Option<ErrorInfo>, error_opt2: Option<ErrorInfo>) -> Result<ErrorInfo, String> {
        if let (None, None) = (&error_opt1, &error_opt2) {
            return Err(format!("None ErrorInfo can't marge"));
        }
        let mut code_start = std::usize::MAX;
        let mut code_end: usize = 0;
        if let Some(err) = error_opt1 {
            code_start = err.get_start();
            code_end = err.get_end();
        }
        if let Some(err) = error_opt2 {
            let tmp_code_start = err.get_start();
            if tmp_code_start < code_start {
                code_start = tmp_code_start;
            }
            let tmp_code_end = err.get_end();
            if tmp_code_end > code_end {
                code_end = tmp_code_end;
            }
        }
        Ok(ErrorInfo::new(code_start, code_end))
    }
    pub fn create_error(&self, input_string: String) -> String {
        let split_input_string: Vec<&str> = input_string.split("\n").collect();
        let split_input_string_until_start: Vec<&str> = (&input_string[0..self.start]).split("\n").collect();
        let start_line_num = split_input_string_until_start.len();
        let start_char_num = split_input_string_until_start[start_line_num-1].len() +1;
        let split_input_string_until_end: Vec<&str> = (&input_string[0..self.end]).split("\n").collect();
        let end_line_num = split_input_string_until_end.len();
        let end_char_num = split_input_string_until_end[end_line_num-1].len() +1;
        
        let mut output_string = String::from("Failed, Compile Error!\n\n");
        output_string += &*format!("==============================================\n");
        if !(self.start == 0 && self.end == 0) {
            output_string += &*format!("Line: {}, Character: {} :\n", start_line_num, start_char_num);
            for i in start_line_num..end_line_num +1 {
                output_string += &*format!("{}\n", split_input_string[i-1]);
                let tmp_len = split_input_string[i-1].len();
                if start_line_num == end_line_num {
                    for j in 0..start_char_num-1 {
                        output_string += &*format!(" ");
                    }
                    for j in start_char_num..end_char_num {
                        output_string += &*format!("^");
                    }
                } else if i == start_line_num {
                    for j in 0..start_char_num-1 {
                        output_string += &*format!(" ");
                    }
                    for j in start_char_num..tmp_len {
                        output_string += &*format!("^");
                    }
                } else if i == end_line_num {
                    for j in 0..end_char_num {
                        output_string += &*format!("^");
                    }
                } else {
                    for j in 0..tmp_len {
                        output_string += &*format!("^");
                    }   
                }
                output_string += &*format!("\n");
            }
        }
        output_string += &*format!("Error: {}\n", self.message);        
        output_string += &*format!("==============================================\n");
        output_string
    }
}